using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Razor;

namespace BeerStore
{
    public class CustomPagesLocationExpander : IViewLocationExpander
    {
        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            yield return "/Views/{1}/{0}.cshtml";
            yield return "/Views/Shared/{0}.cshtml";
            yield return "/Pages/{1}/{0}.cshtml";
            yield return "/Pages/Shared/{0}.cshtml";
        }

        public void PopulateValues(ViewLocationExpanderContext context)
        {            
        }
    }
}