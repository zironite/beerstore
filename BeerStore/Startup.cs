﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BeerStore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BeerStore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationExpanders.Add(new CustomPagesLocationExpander());
            });

            services.AddSession();
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("Sqlite")));
            services.AddDefaultIdentity<User>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<AppDbContext>();

            services.AddMvc(config =>
                {
                    // using Microsoft.AspNetCore.Mvc.Authorization;
                    // using Microsoft.AspNetCore.Authorization;
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                    config.Filters.Add(new AuthorizeFilter(policy));
                }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddSessionStateTempDataProvider();
            
            services.Configure <IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 1;

                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = false;
            });
            
            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(5);

                options.LoginPath = "/Identity/Account/Login";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.SlidingExpiration = true;
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("create", policy => policy.RequireClaim("permission","create"));
                options.AddPolicy("update", policy => policy.RequireClaim("permission","update"));
                options.AddPolicy("delete", policy => policy.RequireClaim("permission","delete"));
                options.AddPolicy("isAdmin", policy => policy.RequireClaim("permission","create")
                    .RequireClaim("permission","update")
                    .RequireClaim("permission","delete"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public async void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "CloseLoginWindow",
                    template: "{controller=Login}/{action=CloseLoginWindow}/{id?}");
            });
            await CreateRoles(app.ApplicationServices);
        }
        
        private async Task CreateRoles(IServiceProvider serviceProvider)
        {
            //initializing custom roles 
            IServiceScopeFactory scopeFactory = serviceProvider.GetRequiredService<IServiceScopeFactory>();

            using (IServiceScope scope = scopeFactory.CreateScope())
            {
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();                
                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<User>>();
                
                string[] roleNames = {"Admin", "Member"};

                foreach (var roleName in roleNames)
                {
                    var roleExist = await roleManager.RoleExistsAsync(roleName);
                    // ensure that the role does not exist
                    if (!roleExist)
                    {
                        //create the roles and seed them to the database: 
                        await roleManager.CreateAsync(new IdentityRole(roleName));
                    }
                }

                string[] adminEmails =
                {
                    "lironpro14@gmail.com",
                    "sa@as.co",
                    "maor1100@gmail.com"
                };

                foreach (var adminEmail in adminEmails)
                {
                    var user = await userManager.FindByEmailAsync(adminEmail);
                    if (user != null)
                    {
                        if (!await userManager.IsInRoleAsync(user, "Admin"))
                        {
                            await userManager.AddToRoleAsync(user, "Admin");
                        }

                        var adminRole = await roleManager.FindByNameAsync("Admin");
                        var claims = await roleManager.GetClaimsAsync(adminRole);
                        if (claims.Count == 0)
                        {
                            await roleManager.AddClaimAsync(adminRole, new Claim("permission", "create"));
                            await roleManager.AddClaimAsync(adminRole, new Claim("permission", "read"));
                            await roleManager.AddClaimAsync(adminRole, new Claim("permission", "update"));
                            await roleManager.AddClaimAsync(adminRole, new Claim("permission", "delete"));
                        }
                        else
                        {
                            var userClaims = await userManager.GetClaimsAsync(user);

                            if (userClaims.Count == 0)
                            {
                                await userManager.AddClaimAsync(user, new Claim("permission", "create"));
                                await userManager.AddClaimAsync(user, new Claim("permission", "read"));
                                await userManager.AddClaimAsync(user, new Claim("permission", "update"));
                                await userManager.AddClaimAsync(user, new Claim("permission", "delete"));
                            }
                        }
                    }
                }
            }
        }
    }
}