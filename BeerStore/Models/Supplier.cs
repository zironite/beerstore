using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerStore.Models
{
    public class Supplier
    {
        public int SupplierId { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        [Required]
        public string Phone { get; set; }
        
        [Required]
        public virtual Address Address { get; set; }
        
        [Required]
        public int AddressKey { get; set; }
        
        [Required]
        public List<ProductSupplier> ProductSuppliers { get; set; }
    }
}