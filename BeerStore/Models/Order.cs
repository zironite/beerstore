using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerStore.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        
        [Required]
        public virtual Address Address { get; set; }
        
        [Required]
        public string Date { get; set; }
        
        [Required]
        public List<OrderProduct> OrderProduct { get; set; }
    }
}