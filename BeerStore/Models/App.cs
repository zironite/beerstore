using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using BeerStore.Models;

namespace BeerStore.Models
{
    public sealed class AppDbContext : IdentityDbContext<User>
    {
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<ProductSupplier> ProductSuppliers { get; set; }
        public DbSet<User> Users { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Address>()
                .HasKey(address => address.AddressKey);
            
//            modelBuilder.Entity<Address>()
//                .HasMany(address => address.Suppliers)
//                .WithOne(supplier => supplier.Address);
            
            modelBuilder.Entity<Product>()
                .HasKey(product => product.ProductId);
            
            modelBuilder.Entity<ProductSupplier>()
                .HasKey(productSupplier =>
                    new
                    {
                        productSupplier.ProductId,
                        productSupplier.SupplierId
                    });
            modelBuilder.Entity<Supplier>()
                .HasKey(supplier => supplier.SupplierId);
            modelBuilder.Entity<ProductSupplier>()
                .HasOne(productsSupplier => productsSupplier.Product)
                .WithMany(product => product.ProductSuppliers)
                .HasForeignKey(productSupplier => productSupplier.ProductId);
            
            modelBuilder.Entity<ProductSupplier>()
                .HasOne(productsSupplier => productsSupplier.Supplier)
                .WithMany(supplier => supplier.ProductSuppliers)
                .HasForeignKey(productSupplier => productSupplier.SupplierId);
            
            modelBuilder.Entity<Customer>()
                .HasKey(customer => customer.CustomerId);
            
            modelBuilder.Entity<Order>()
                .HasKey(order => order.OrderId);
            
            modelBuilder.Entity<OrderProduct>()
                .HasKey(orderProduct =>
                    new
                    {
                        orderProduct.OrderId,
                        orderProduct.ProductId
                    });
            modelBuilder.Entity<OrderProduct>()
                .HasOne(orderProduct => orderProduct.Order)
                .WithMany(order=> order.OrderProduct)
                .HasForeignKey(customerOrder => customerOrder.OrderId);

            modelBuilder.Entity<OrderProduct>()
                .HasOne(orderProduct => orderProduct.Product)
                .WithMany(product=> product.OrderProduct)
                .HasForeignKey(customerOrder => customerOrder.ProductId);

            modelBuilder.Entity<User>()
                .HasOne(user => user.Customer)
                .WithOne(customer => customer.User)
                .HasForeignKey<User>(user => user.CustomerId);

        }

        public DbSet<BeerStore.Models.Customer> Customer { get; set; }

        public DbSet<BeerStore.Models.Order> Order { get; set; }
    }
}