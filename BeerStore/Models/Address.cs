using System.ComponentModel.DataAnnotations;

namespace BeerStore.Models
{
    public class Address
    {
        public int AddressKey { get; set; }
        
        [Required]
        public string City { get; set; }
        
        [Required]
        public string Street { get; set; }
        
        [Required]
        public int Number { get; set; }
        
        [Required]
        public int ZipCode { get; set; }
        
//        [Required]
//        public List<Supplier> Suppliers { get; set; }
        
    }
}