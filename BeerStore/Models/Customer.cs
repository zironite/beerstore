using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerStore.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        
        [Required]
        public string FirstName { get; set; }
        
        [Required]
        public string LastName { get; set; }
        
        [Required]
        public string Phone { get; set; }
        
        [Required]
        public virtual Address Address { get; set; }
        
        [Required]
        public string BirthDate { get; set; }
        
        [Required]
        public GenderType Gender { get; set; }
        
        [Required]
        public User User { get; set; }
    }
    
    
    public enum GenderType
    {
        Male=1,
        Female=2
    }
}