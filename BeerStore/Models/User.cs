using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace BeerStore.Models
{
    public class User : IdentityUser
    {
        [Required]
        public int CustomerId { get; set; }
        
        [Required]
        public Customer Customer { get; set; }
    }
}