using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerStore.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        [Required]
        public float Price { get; set; }
        
        [Required]
        public int Volume { get; set; }
        
        [Required]
        public string Description { get; set; }
        
        public string Image { get; set; } 
        
        public List<ProductSupplier> ProductSuppliers { get; set; }
        
        public List<OrderProduct> OrderProduct { get; set; }
    }
}