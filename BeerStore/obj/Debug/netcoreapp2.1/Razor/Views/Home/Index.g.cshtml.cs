#pragma checksum "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6b482770557b7cb0ac9d00f75750fd12ecbe7ae0"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/_ViewImports.cshtml"
using BeerStore;

#line default
#line hidden
#line 2 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/_ViewImports.cshtml"
using BeerStore.Models;

#line default
#line hidden
#line 1 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
using Newtonsoft.Json.Linq;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6b482770557b7cb0ac9d00f75750fd12ecbe7ae0", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f00ca1229d96120620204ed5d0af5d897ac59b6e", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/videos/main_video.mp4"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", new global::Microsoft.AspNetCore.Html.HtmlString("video/mp4"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(103, 1, true);
            WriteLiteral("\n");
            EndContext();
#line 5 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
            BeginContext(146, 1, true);
            WriteLiteral("\n");
            EndContext();
            BeginContext(399, 1, true);
            WriteLiteral("\n");
            EndContext();
#line 18 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
 if (IsCurrentUserAdmin())
{

#line default
#line hidden
            BeginContext(429, 216, true);
            WriteLiteral("    <script type=\"text/javascript\">\n        document.addEventListener(\'DOMContentLoaded\', function() {\n            createOrderPerMonthChart();\n            createProfitabilityChart();\n        }, false);\n    </script>\n");
            EndContext();
            BeginContext(650, 105, true);
            WriteLiteral("    <header>\n        <h2>Manageable items</h2>\n    </header>\n    <nav class=\"nav nav-pills\">\n        <li>");
            EndContext();
            BeginContext(756, 49, false);
#line 31 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
       Write(Html.ActionLink("Customers", "Index", "Customer"));

#line default
#line hidden
            EndContext();
            BeginContext(805, 18, true);
            WriteLiteral("</li>\n        <li>");
            EndContext();
            BeginContext(824, 43, false);
#line 32 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
       Write(Html.ActionLink("Orders", "Index", "Order"));

#line default
#line hidden
            EndContext();
            BeginContext(867, 18, true);
            WriteLiteral("</li>\n        <li>");
            EndContext();
            BeginContext(886, 47, false);
#line 33 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
       Write(Html.ActionLink("Products", "Index", "Product"));

#line default
#line hidden
            EndContext();
            BeginContext(933, 18, true);
            WriteLiteral("</li>\n        <li>");
            EndContext();
            BeginContext(952, 49, false);
#line 34 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
       Write(Html.ActionLink("Suppliers", "Index", "Supplier"));

#line default
#line hidden
            EndContext();
            BeginContext(1001, 438, true);
            WriteLiteral(@"</li>
    </nav>
    <hr/>
    <aside>
        <header>
            <h2>Store Statistics</h2>
        </header>
        <table>
            <tr align=""center"">
                <td><label>Number of order by day of the month</label></td>
                <td><label>Profit By product</label></td>
            </tr>
            <tr align=""center"">
                <td>
                    <div id=""orderByMonthChart"">
                        ");
            EndContext();
            BeginContext(1440, 23, false);
#line 49 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
                   Write(Html.AntiForgeryToken());

#line default
#line hidden
            EndContext();
            BeginContext(1463, 149, true);
            WriteLiteral("\n                    </div>\n                </td>\n                <td>\n                    <div id=\"profitabilityByProduct\">\n                        ");
            EndContext();
            BeginContext(1613, 23, false);
#line 54 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
                   Write(Html.AntiForgeryToken());

#line default
#line hidden
            EndContext();
            BeginContext(1636, 98, true);
            WriteLiteral("\n                    </div>\n                </td>\n            </tr>\n        </table>\n    </aside>\n");
            EndContext();
#line 60 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
}
else
{

#line default
#line hidden
            BeginContext(1743, 80, true);
            WriteLiteral("    <br/><center>\n        <video width=\"854\" height=\"480\" controls>\n            ");
            EndContext();
            BeginContext(1823, 56, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("source", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "09f2716a280d43a39a010989ac6f7c62", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1879, 173, true);
            WriteLiteral("\n        </video>\n    </center><br/>\n    <div class=\"well well-lg\">\n        <h2>Random Beer Section</h2>\n        <dl class=\"dl-horizontal\">\n            <dt>\n                ");
            EndContext();
            BeginContext(2053, 24, false);
#line 72 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
           Write(Html.DisplayName("Name"));

#line default
#line hidden
            EndContext();
            BeginContext(2077, 52, true);
            WriteLiteral("\n            </dt>\n            <dd>\n                ");
            EndContext();
            BeginContext(2131, 50, false);
#line 75 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
            Write(((JObject) ViewData["RandomBeer"])["data"]["name"]);

#line default
#line hidden
            EndContext();
            BeginContext(2182, 52, true);
            WriteLiteral("\n            </dd>\n            <dt>\n                ");
            EndContext();
            BeginContext(2235, 30, false);
#line 78 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
           Write(Html.DisplayName("Percentage"));

#line default
#line hidden
            EndContext();
            BeginContext(2265, 52, true);
            WriteLiteral("\n            </dt>\n            <dd>\n                ");
            EndContext();
            BeginContext(2319, 49, false);
#line 81 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
            Write(((JObject) ViewData["RandomBeer"])["data"]["abv"]);

#line default
#line hidden
            EndContext();
            BeginContext(2369, 52, true);
            WriteLiteral("\n            </dd>\n            <dt>\n                ");
            EndContext();
            BeginContext(2422, 25, false);
#line 84 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
           Write(Html.DisplayName("Style"));

#line default
#line hidden
            EndContext();
            BeginContext(2447, 52, true);
            WriteLiteral("\n            </dt>\n            <dd>\n                ");
            EndContext();
            BeginContext(2501, 59, false);
#line 87 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
            Write(((JObject) ViewData["RandomBeer"])["data"]["style"]["name"]);

#line default
#line hidden
            EndContext();
            BeginContext(2561, 52, true);
            WriteLiteral("\n            </dd>\n            <dt>\n                ");
            EndContext();
            BeginContext(2614, 37, false);
#line 90 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
           Write(Html.DisplayName("Style description"));

#line default
#line hidden
            EndContext();
            BeginContext(2651, 84, true);
            WriteLiteral("\n            </dt>\n            <dd>\n                <div class=\"multiple-columns-3\">");
            EndContext();
            BeginContext(2737, 66, false);
#line 93 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
                                            Write(((JObject) ViewData["RandomBeer"])["data"]["style"]["description"]);

#line default
#line hidden
            EndContext();
            BeginContext(2804, 50, true);
            WriteLiteral("</div>\n            </dd>\n        </dl>\n    </div>\n");
            EndContext();
#line 97 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
#line 10 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Home/Index.cshtml"
 
    bool IsCurrentUserAdmin()
    {
        var currentUser = UserManager.GetUserAsync(User).GetAwaiter().GetResult();
        return currentUser != null && UserManager.IsInRoleAsync(currentUser, "Admin").GetAwaiter().GetResult();
    }

#line default
#line hidden
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public UserManager<User> UserManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
