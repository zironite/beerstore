#pragma checksum "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e9a5084fc0ecd77eaa088952da41e2c17f816dd7"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Product_Index), @"mvc.1.0.view", @"/Views/Product/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Product/Index.cshtml", typeof(AspNetCore.Views_Product_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/_ViewImports.cshtml"
using BeerStore;

#line default
#line hidden
#line 2 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/_ViewImports.cshtml"
using BeerStore.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e9a5084fc0ecd77eaa088952da41e2c17f816dd7", @"/Views/Product/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f00ca1229d96120620204ed5d0af5d897ac59b6e", @"/Views/_ViewImports.cshtml")]
    public class Views_Product_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<BeerStore.Models.Product>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Create", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Details", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(45, 25, true);
            WriteLiteral("\n<!DOCTYPE html>\n\n<html>\n");
            EndContext();
            BeginContext(70, 97, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "5578d87b30604ba098ac5afd83ae5151", async() => {
                BeginContext(76, 84, true);
                WriteLiteral("\n    <meta name=\"viewport\" content=\"width=device-width\" />\n    <title>Index</title>\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(167, 1, true);
            WriteLiteral("\n");
            EndContext();
            BeginContext(168, 3936, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7e24b6c6d42344578867f856e850889a", async() => {
                BeginContext(174, 2, true);
                WriteLiteral("\n\n");
                EndContext();
#line 12 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
 if (User != null && User.HasClaim("permission","create"))
{

#line default
#line hidden
                BeginContext(237, 16, true);
                WriteLiteral("    <p>\n        ");
                EndContext();
                BeginContext(253, 37, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "462e2d2e684f4ea197cb0d6d3f3993ca", async() => {
                    BeginContext(276, 10, true);
                    WriteLiteral("Create New");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(290, 10, true);
                WriteLiteral("\n    </p>\n");
                EndContext();
#line 17 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
}

#line default
#line hidden
                BeginContext(302, 88, true);
                WriteLiteral("<div class=\"form-group\">\n    <br/>\n    <label class=\"control-label\">Search</label><br/>\n");
                EndContext();
#line 21 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
     using (Html.BeginForm("Index", "Product", FormMethod.Get))
    {

#line default
#line hidden
                BeginContext(460, 26, true);
                WriteLiteral("        <div>\n            ");
                EndContext();
                BeginContext(487, 221, false);
#line 24 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
       Write(Html.TextBox("name", ViewBag.CurrentFilter as string, new
            {
                @style = "width: 69%; display: inline",
                @class = "form-control",
                @placeholder = "Name"
            }));

#line default
#line hidden
                EndContext();
                BeginContext(708, 682, true);
                WriteLiteral(@"
            <a value=""Search"" id=""extra-button"" style=""display: inline; width: 10%"">Extra filters ▼</a>
            <input type=""submit"" value=""Search"" class=""form-control"" style=""width: 10%; display: inline""/>
        </div>
        <div class=""form-group"" id=""extra-filter"" hidden=""true"">
            <br/>
            <table style=""width: 100%"">
                <tr>
                    <td>
                        <label class=""control-label"">Volume</label>
                    </td>
                    <td>
                        <label class=""control-label"">Price Range</label>
                    </td>
                </tr>
                <tr>
                    <td>
");
                EndContext();
#line 46 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
                         foreach (var volume in ViewBag.Volumes)
                        {

#line default
#line hidden
                BeginContext(1481, 164, true);
                WriteLiteral("                            <div name=\"checkboxs\">\n                                <label>\n                                    <input type=\"checkbox\" name=\"volumes\"");
                EndContext();
                BeginWriteAttribute("value", " value=\"", 1645, "\"", 1660, 1);
#line 50 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
WriteAttributeValue("", 1653, volume, 1653, 7, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(1661, 2, true);
                WriteLiteral("> ");
                EndContext();
                BeginContext(1664, 6, false);
#line 50 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
                                                                                      Write(volume);

#line default
#line hidden
                EndContext();
                BeginContext(1670, 77, true);
                WriteLiteral("\n                                </label>\n                            </div>\n");
                EndContext();
#line 53 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
                        }

#line default
#line hidden
                BeginContext(1773, 75, true);
                WriteLiteral("                    </td>\n                    <td>\n                        ");
                EndContext();
                BeginContext(1849, 261, false);
#line 56 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
                   Write(Html.TextBox("minRange", "", new
                        {
                            @style = "width: 10%; display: inline",
                            @class = "form-control",
                            @placeholder = "Min price"
                        }));

#line default
#line hidden
                EndContext();
                BeginContext(2110, 64, true);
                WriteLiteral("\n                        <span>-</span>\n                        ");
                EndContext();
                BeginContext(2175, 261, false);
#line 63 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
                   Write(Html.TextBox("maxRange", "", new
                        {
                            @style = "width: 10%; display: inline",
                            @class = "form-control",
                            @placeholder = "Max price"
                        }));

#line default
#line hidden
                EndContext();
                BeginContext(2436, 85, true);
                WriteLiteral("\n                    </td>\n                </tr>\n            </table>\n        </div>\n");
                EndContext();
#line 73 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
    }

#line default
#line hidden
                BeginContext(2527, 87, true);
                WriteLiteral("</div>\n<table class=\"table\">\n    <thead>\n        <tr>\n            <th>\n                ");
                EndContext();
                BeginContext(2615, 40, false);
#line 79 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Name));

#line default
#line hidden
                EndContext();
                BeginContext(2655, 52, true);
                WriteLiteral("\n            </th>\n            <th>\n                ");
                EndContext();
                BeginContext(2708, 41, false);
#line 82 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Price));

#line default
#line hidden
                EndContext();
                BeginContext(2749, 52, true);
                WriteLiteral("\n            </th>\n            <th>\n                ");
                EndContext();
                BeginContext(2802, 42, false);
#line 85 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Volume));

#line default
#line hidden
                EndContext();
                BeginContext(2844, 52, true);
                WriteLiteral("\n            </th>\n            <th>\n                ");
                EndContext();
                BeginContext(2897, 47, false);
#line 88 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Description));

#line default
#line hidden
                EndContext();
                BeginContext(2944, 80, true);
                WriteLiteral("\n            </th>\n            <th></th>\n        </tr>\n    </thead>\n    <tbody>\n");
                EndContext();
#line 94 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
 foreach (var item in Model) {

#line default
#line hidden
                BeginContext(3055, 65, true);
                WriteLiteral("        <tr>\n            <td style=\"width: 21%\">\n                ");
                EndContext();
                BeginContext(3121, 39, false);
#line 97 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Name));

#line default
#line hidden
                EndContext();
                BeginContext(3160, 70, true);
                WriteLiteral("\n            </td>\n            <td style=\"width: 2%\">\n                ");
                EndContext();
                BeginContext(3231, 40, false);
#line 100 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Price));

#line default
#line hidden
                EndContext();
                BeginContext(3271, 70, true);
                WriteLiteral("\n            </td>\n            <td style=\"width: 2%\">\n                ");
                EndContext();
                BeginContext(3342, 41, false);
#line 103 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Volume));

#line default
#line hidden
                EndContext();
                BeginContext(3383, 74, true);
                WriteLiteral("\n            </td>\n            <td style=\"width: 52.41%\">\n                ");
                EndContext();
                BeginContext(3458, 46, false);
#line 106 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Description));

#line default
#line hidden
                EndContext();
                BeginContext(3504, 36, true);
                WriteLiteral("\n            </td>\n            <td>\n");
                EndContext();
#line 109 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
                 if (User != null && User.HasClaim("permission","update"))
                {

#line default
#line hidden
                BeginContext(3633, 20, true);
                WriteLiteral("                    ");
                EndContext();
                BeginContext(3653, 60, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f38881c953e14c47a5d1e5122594fd3e", async() => {
                    BeginContext(3705, 4, true);
                    WriteLiteral("Edit");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
                if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
                {
                    throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
                }
                BeginWriteTagHelperAttribute();
#line 111 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
                                           WriteLiteral(item.ProductId);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3713, 16, true);
                WriteLiteral(" <span>|</span>\n");
                EndContext();
#line 112 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
                }

#line default
#line hidden
                BeginContext(3747, 16, true);
                WriteLiteral("                ");
                EndContext();
                BeginContext(3763, 66, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a72d1e5ad8a244bbbd162779a2af9c26", async() => {
                    BeginContext(3818, 7, true);
                    WriteLiteral("Details");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
                if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
                {
                    throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
                }
                BeginWriteTagHelperAttribute();
#line 113 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
                                          WriteLiteral(item.ProductId);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3829, 1, true);
                WriteLiteral("\n");
                EndContext();
#line 114 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
                 if (User != null && User.HasClaim("permission","update"))
                {

#line default
#line hidden
                BeginContext(3923, 35, true);
                WriteLiteral("                    <span>|</span> ");
                EndContext();
                BeginContext(3958, 64, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "56dedef96d70425ca8a7f5b34ab80702", async() => {
                    BeginContext(4012, 6, true);
                    WriteLiteral("Delete");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
                if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
                {
                    throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
                }
                BeginWriteTagHelperAttribute();
#line 116 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
                                                            WriteLiteral(item.ProductId);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4022, 1, true);
                WriteLiteral("\n");
                EndContext();
#line 117 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
                }

#line default
#line hidden
                BeginContext(4041, 32, true);
                WriteLiteral("            </td>\n        </tr>\n");
                EndContext();
#line 120 "/mnt/linux2/home/liron/RiderProjects/BeerStore/BeerStore/Views/Product/Index.cshtml"
}

#line default
#line hidden
                BeginContext(4075, 22, true);
                WriteLiteral("    </tbody>\n</table>\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4104, 9, true);
            WriteLiteral("\n</html>\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<BeerStore.Models.Product>> Html { get; private set; }
    }
}
#pragma warning restore 1591
