﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.


function onLoginClick() {
    var options = {
        backdrop: "static",
        keyboard: true,
        show: true
    };

    $("#loginModal").modal(options);
}

function onLoginCloseBtn() {
    
    // Hide the modal
    $("#loginModal").modal('hide');
    
    // Reset the iframe to it's original source
    $("#loginModal iframe").attr("src", "/Identity/Account/Login");
}

function onLoginFrameLoad() {
    if($("#loginModal iframe")[0].contentDocument.title === "Home Page - BeerStore") {
        onLoginCloseBtn();
        location.reload(true);
    }
}

var map;

function LoadMap() {
    map = new Microsoft.Maps.Map('#map', {
        credentials: "AudtxCO7C5RXyrsW9CAtVEuXpfFUFT_i8fBafiihY9V18emEmlghhSOUjVTDoK1_ ",
        zoom: 7,
        center: new Microsoft.Maps.Location('31.45', '35.203165854')
    });
}

function loadJSON(Name,Address) {
    $.ajax({
        type: "GET",
        url: "https://dev.virtualearth.net/REST/v1/Locations?query=" + Address + "&key=AudtxCO7C5RXyrsW9CAtVEuXpfFUFT_i8fBafiihY9V18emEmlghhSOUjVTDoK1_",
        success: function (data) {
            var alt = data.resourceSets[0].resources[0].point.coordinates[0];
            var long = data.resourceSets[0].resources[0].point.coordinates[1];
            var pushpin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(alt, long),
                {
                    text: Name
                });
            map.entities.push(pushpin);
        },
        error: function () {
            alert("Map content load failed.");
        }
    });
}
     
function createOrderPerMonthChart() {
    var margin = {top: 20, right: 20, bottom: 70, left: 40},
        width = 600 - margin.left - margin.right,
        height = 300 - margin.top - margin.bottom;

    var x = d3.scaleBand().range([0, width]).round(0.05);

    var y = d3.scaleLinear().range([height, 0]);

    var xAxis = d3.axisBottom(x);

    var yAxis = d3.axisLeft(y)
        .ticks(10);

    var svg = d3.select("#orderByMonthChart").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    $.ajax({
        type: "GET",
        url: "/Order/ByMonth",
        headers: {
            RequestVerificationToken:
                $('input:hidden[name="__RequestVerificationToken"]').val()
        },
        success: function (data) {
            x.domain(data.map(function(d) { return d.key; }));
            y.domain([0, d3.max(data, function(d) { return d.value; })]);

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis)
                .selectAll("text")
                .style("text-anchor", "end")
                .attr("dx", "-.8em")
                .attr("dy", "-.55em")
                .attr("transform", "rotate(-90)" );

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text("Value ($)");

            svg.selectAll("bar")
                .data(data)
                .enter().append("rect")
                .style("fill", "steelblue")
                .attr("x", function(d) { return x(d.key) + x.bandwidth() / 4; })
                .attr("width", x.bandwidth() / 2)
                .attr("y", function(d) { return y(d.value); })
                .attr("height", function(d) { return height - y(d.value); });
        },
        error: function() {
        }
    });
}

function loadImageData() {
    var reader = new FileReader();
    reader.readAsDataURL($('#filechooser')[0].files[0]);
    reader.onload = function () {
        document.getElementById("imageData").value = reader.result;
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };    
}

function createProfitabilityChart() {
    var margin = {top: 20, right: 20, bottom: 70, left: 40},
        width = 600 - margin.left - margin.right,
        height = 300 - margin.top - margin.bottom;

    var x = d3.scaleBand().range([0, width]).round(0.05);

    var y = d3.scaleLinear().range([height, 0]);

    var xAxis = d3.axisBottom(x);

    var yAxis = d3.axisLeft(y)
        .ticks(10);

    var svg = d3.select("#profitabilityByProduct").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    $.ajax({
        type: "GET",
        url: "/Product/ProfitabilityByProduct",
        headers: {
            RequestVerificationToken:
                $('input:hidden[name="__RequestVerificationToken"]').val()
        },
        success: function (data) {
            x.domain(data.map(function(d) { return d.key; }));
            y.domain([0, d3.max(data, function(d) { return d.value; })]);

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis)
                .selectAll("text")
                .style("text-anchor", "end")
                .attr("dx", "-.8em")
                .attr("dy", "-.55em")
                .attr("transform", "rotate(-90)" );

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text("Value ($)");

            svg.selectAll("bar")
                .data(data)
                .enter().append("rect")
                .style("fill", "steelblue")
                .attr("x", function(d) { return x(d.key) + x.bandwidth() / 4; })
                .attr("width", x.bandwidth() / 2)
                .attr("y", function(d) { return y(d.value); })
                .attr("height", function(d) { return height - y(d.value); });
        },
        error: function() {
        }
    });
}

$('#extra-button').click(function() {
    $('#extra-filter').toggle();
});

function loadToCanvas(src){
    var canvas = document.getElementById("imageCanvas");
    var ctx = canvas.getContext("2d");
    var img = document.getElementById("realimage");
    var ratio;
    if (img.width > img.height) {
        ratio = canvas.width / img.width;
    } else {
        ratio = canvas.height / img.height;
    }
    ctx.drawImage(img, 0, 0, img.width * ratio, img.height * ratio);
    img.src="";
}