using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BeerStore.Models;
using Microsoft.AspNetCore.Authorization;
using NReco.CF.Taste.Impl.Model.File;
using NReco.CF.Taste.Impl.Neighborhood;
using NReco.CF.Taste.Impl.Recommender;
using NReco.CF.Taste.Impl.Similarity;
using RestSharp;

namespace BeerStore.Controllers
{
    public class ProductController : Controller
    {
        private readonly AppDbContext _context;

        public ProductController(AppDbContext context)
        {
            _context = context;
        }

        [AllowAnonymous]
        // GET: Product
        public async Task<IActionResult> Index([FromQuery(Name = "name")] string nameQuery,
            [FromQuery(Name = "volumes")] List<int> volumesQuery,
            [FromQuery(Name = "minRange")] float minRangeQuery,
            [FromQuery(Name = "maxRange")] float maxRangeQuery)
        {
            ViewData["Volumes"] = _context.Products.Select(p => p.Volume).Distinct().ToList();
            
            if (String.IsNullOrEmpty(nameQuery) && (volumesQuery == null || volumesQuery.Count == 0) &&
                (!Request.Query.ContainsKey("minRange") || Request.Query["minRange"][0] == "") && 
                (!Request.Query.ContainsKey("maxRange") || Request.Query["maxRange"][0] == ""))
            {
                return View(await _context.Products.ToListAsync());
            }

            var matchingProducts = _context.Products.Where(p => String.IsNullOrEmpty(nameQuery) ||
                                                                p.Name.Contains(nameQuery))
                .Where(p => volumesQuery == null || volumesQuery.Count == 0 ||
                            volumesQuery.Contains(p.Volume))
                .Where(p => !Request.Query.ContainsKey("minRange") || Request.Query["minRange"][0] == "" ||
                             p.Price >= minRangeQuery)
                .Where(p => !Request.Query.ContainsKey("maxRange") || Request.Query["maxRange"][0] == "" ||
                            p.Price <= maxRangeQuery);
            
            return View(await matchingProducts.ToListAsync());
        }

        [AllowAnonymous]
        // GET: Product/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }
            
            var user = _context.Users.Include(u => u.Customer)
                .FirstOrDefault(u => u.UserName == HttpContext.User.Identity.Name);
            if (user != null)
            {
                var customerId = user.Customer.CustomerId;
                WriteUserToProductToCsv();
                var model = new FileDataModel("data.csv");
                var similarity = new LogLikelihoodSimilarity(model);
                var neighborhood = new ThresholdUserNeighborhood(0.001, similarity, model);
                var recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);
                var items = recommender.Recommend(customerId, 5).Select(r => r.GetItemID()).ToList();
                var recommendedItems = _context.Products
                    .Where(p => items.Contains(p.ProductId)).ToList();
                ViewData["RecommendedProducts"] = recommendedItems;
            }

            return View(product);
        }

        [Authorize(Policy = "create")]
        // GET: Product/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Policy = "create")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProductId,Name,Price,Volume,Description,Image")] Product product)
        {
            if (ModelState.IsValid)
            {
                _context.Add(product);
                await _context.SaveChangesAsync();
                var fbAccessToken =
                    "EAAcCYQnKvRoBAA8bFsRr5ZAl6NiYxdmgP3cX244qcKlKJnea27f6iDfZA7LaKqcL07vZAQGUetzN2AQMjZAonTH7i0TZCf4WRhAVIdGV7EMRklJQHkZCeyerYSyrWOovZABubsHy0EBFRUB9vRBi1oeszgZA5F5xNk80QjxCoPG3VQZDZD";
                var pageId = "317258652420182";
                var client = new RestClient("https://graph.facebook.com");
                var request = new RestRequest($"/{pageId}/feed", Method.POST);
                request.AddQueryParameter("message", $"Another fancy beer called {product.Name} was added to our website!");
                request.AddQueryParameter("access_token", fbAccessToken);
                var response = client.Execute(request);
                if (!response.IsSuccessful) throw new Exception("Product addition was not published on Facebook successfully");
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Product/Edit/5
        [Authorize(Policy = "update")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Policy = "update")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProductId,Name,Price,Volume,Description")] Product product)
        {
            if (id != product.ProductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(product);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.ProductId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Product/Delete/5
        [Authorize(Policy = "delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Product/Delete/5
        [Authorize(Policy = "delete")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var product = await _context.Products.FindAsync(id);
            try
            {
                _context.Products.Remove(product);
            }
            catch (Exception e)
            {
                //Noop
            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.ProductId == id);
        }

        [HttpPost, ActionName("AddToCart")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddToCart(int id)
        {
            var product = await _context.Products.FindAsync(id);
            var cart = SessionHelper.GetObjectFromJson<Order>(HttpContext.Session, "cart");

            if (cart == null)
            {
                cart = new Order()
                {
                    OrderProduct = new List<OrderProduct>()
                };
            }

            var existingOrderProduct = cart.OrderProduct
                .FirstOrDefault(currOrderProduct => currOrderProduct.Product.ProductId == id);
            if (existingOrderProduct != null)
            {
                existingOrderProduct.Quantity++;
            }
            else
            {
                var orderProduct = new OrderProduct
                {
                    Product = product,
                    Quantity = 1
                };
                cart.OrderProduct.Add(orderProduct);
            }

            SessionHelper.SetObjectAsJson(HttpContext.Session,"cart",cart);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet, ActionName("ProfitabilityByProduct")]
        [ValidateAntiForgeryToken]
        public List<KeyValuePair<string, int>> ProfitabilityByProduct()
        {
            return _context.Order.Include(o => o.OrderProduct).ThenInclude(op => op.Product)
                .SelectMany(o => o.OrderProduct).GroupBy(op => op.Product.Name)
                .Select(row => new KeyValuePair<string, int>(row.Key,
                    (int) row.Sum(selector => selector.Quantity * selector.Product.Price))).ToList();
        }

        public void WriteUserToProductToCsv()
        {
            var customerToUser = _context.Order.Include(o => o.OrderProduct).ThenInclude(op => op.Product)
                .Join(_context.Customer,
                    o => $"{o.Address.City},{o.Address.Number},{o.Address.Street},{o.Address.ZipCode}",
                    o => $"{o.Address.City},{o.Address.Number},{o.Address.Street},{o.Address.ZipCode}",
                    (o, c) =>
                        new Tuple<int, List<int>>(c.CustomerId, o.OrderProduct.Select(op =>
                            op.Product.ProductId).ToList()))
                .SelectMany(t => t.Item2.Select(p => new Tuple<int, int>(t.Item1, p)))
                .Union(_context.Products.Select(p => new Tuple<int,int>(0,p.ProductId))).ToList();
            var csvContent = String.Join("\n", customerToUser.Select(t => $"{t.Item1},{t.Item2}"));
            System.IO.File.WriteAllText("data.csv",csvContent);
        }
    }
}
