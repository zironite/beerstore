﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using BeerStore.Models;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace BeerStore.Controllers
{
    public class HomeController : Controller
    {
        private readonly AppDbContext _dbContext;

        public HomeController(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        [AllowAnonymous]
        public IActionResult Index()
        {
            var client =
                new RestClient("https://sandbox-api.brewerydb.com");
            var request = new RestRequest("/v2/beer/random", Method.GET, DataFormat.Json);
            request.AddQueryParameter("key", "30fc80efcc3255813e343089dd06ace7");
            var response = client.Execute(request);
            var responseJson = JObject.Parse(response.Content);
            ViewData["RandomBeer"] = responseJson;
            return View();
        }

        [AllowAnonymous]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}