using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BeerStore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Query.Internal;
using NReco.CF.Taste.Impl.Common;
using NReco.CF.Taste.Impl.Model;
using NReco.CF.Taste.Impl.Model.File;
using NReco.CF.Taste.Impl.Neighborhood;
using NReco.CF.Taste.Impl.Recommender;
using NReco.CF.Taste.Impl.Similarity;
using NReco.CF.Taste.Model;

namespace BeerStore.Controllers
{
    public class OrderController : Controller
    {
        private readonly AppDbContext _context;

        public OrderController(AppDbContext context)
        {
            _context = context;
        }

        // GET: Order
        public async Task<IActionResult> Index([FromQuery(Name = "OrderId")] string orderId)
        {
            var matchingOrders = _context.Order.AsQueryable();
            if (!String.IsNullOrEmpty(orderId)) matchingOrders = matchingOrders.Where(x => x.OrderId.Equals(int.Parse(orderId)));

            return View(await matchingOrders.ToListAsync());
        }

        // GET: Order/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                var cart = SessionHelper.GetObjectFromJson<Order>(HttpContext.Session, "cart");
                
                if (cart == null)
                {
                    cart = new Order()
                    {
                        OrderProduct = new List<OrderProduct>()
                    };
                }

                return View(cart);
            }

            var order = await _context.Order
                .Include(currOrder => currOrder.Address)
                .Include(currOrder => currOrder.OrderProduct)
                .ThenInclude(orderProduct => orderProduct.Product)
                .FirstOrDefaultAsync(m => m.OrderId == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }
        
        // POST: Order/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create()
        {
            var order = SessionHelper.GetObjectFromJson<Order>(HttpContext.Session, "cart");
            if (ModelState.IsValid && order != null)
            {
                order.OrderProduct = order.OrderProduct.Select(orderProduct => new OrderProduct()
                {
                    Order = null,
                    OrderId = orderProduct.OrderId,
                    Product = null,
                    ProductId = orderProduct.Product.ProductId,
                    Quantity = orderProduct.Quantity
                }).ToList();
                order.Date = DateTime.Now.ToString("dd/MM/yyyy");
                order.Address = _context.Users.Include(user => user.Customer).ThenInclude(customer => customer.Address)
                    .FirstOrDefault(user => user.UserName == User.Identity.Name)
                    ?.Customer.Address;
                _context.Add(order);
                await _context.SaveChangesAsync();
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", null);
                return RedirectToAction("Index", "Home");
            }
            return View(order);
        }

        // GET: Order/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            return View(order);
        }

        // POST: Order/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("OrderId,Date")] Order order)
        {
            if (id != order.OrderId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.OrderId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(order);
        }

        // GET: Order/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order
                .FirstOrDefaultAsync(m => m.OrderId == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Order/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (id == 0)
            {
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", null);
                return RedirectToAction("Details");
            }
            
            var order = await _context.Order.FindAsync(id);
            try
            {
                _context.Order.Remove(order);
            }
            catch (Exception e)
            {
                //Noop
            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderExists(int id)
        {
            return _context.Order.Any(e => e.OrderId == id);
        }

        [HttpGet, ActionName("ByMonth")]
        [ValidateAntiForgeryToken]
        public List<KeyValuePair<string, int>> OrdersByMonth()
        {
            var ordersByMonth = _context.Order.GroupBy(keySelector => 
                DateTime.Parse(keySelector.Date,CultureInfo.CurrentCulture).ToString("MM-yyyy")).Select(row =>
                new KeyValuePair<string,int>(row.Key,row.Count())).ToList();
            return ordersByMonth;
        }
    }
}
