using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BeerStore.Models;
using Microsoft.EntityFrameworkCore.Internal;

namespace BeerStore.Controllers
{
    public class SupplierController : Controller
    {
        private readonly AppDbContext _context;

        public SupplierController(AppDbContext context)
        {
            _context = context;
        }
        
        public class EditModel
        {
            public int SupplierId { get; set; }
            
            [DataType(DataType.Text)]
            [Display(Name = "Supplier Name")]
            [StringLength(100, 
                ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", 
                MinimumLength = 2)]
            public string Name { get; set; }
            
            [DataType(DataType.PhoneNumber)]
            [Display(Name = "Phone")]
            public string Phone { get; set; }
            
            [DataType(DataType.Text)]
            [Display(Name = "City")]
            [StringLength(100, 
                ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", 
                MinimumLength = 2)]
            public string City { get; set; }
            
            [DataType(DataType.Text)]
            [Display(Name = "Street")]
            [StringLength(100, 
                ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", 
                MinimumLength = 2)]
            public string Street { get; set; }
            
            [DataType(DataType.Text)]
            [Display(Name = "Home number")]
            public int HomeNumber { get; set; }
            
            [DataType(DataType.PostalCode)]
            [Display(Name = "Zip code")]
            public int ZipCode { get; set; }
            
            [Display(Name = "Products")]
            public List<int> Products { get; set; }
            
        }

        // GET: Supplier
        public async Task<IActionResult> Index(string searchString, string searchIndex)
        {
            var appDbContext = _context.Suppliers.Include(s => s.Address).AsQueryable();
            
            if (! String.IsNullOrEmpty(searchString))
            {
                switch (searchIndex)
                {
                    case "phone":
                        appDbContext = appDbContext.Where(s => s.Phone.ToLower().Contains(searchString.ToLower()));
                        break;
                    case "city":
                        appDbContext = appDbContext.Where(s => s.Address.City.ToLower().Contains(searchString.ToLower()));
                        break;
                    case "product":
                        appDbContext = appDbContext.Include(s => s.ProductSuppliers)
                            .Where(s => s.ProductSuppliers.Exists(p => p.Product.Name.ToLower().Contains(searchString.ToLower())));
                        break;
                    default:
                        appDbContext = appDbContext.Where(s => s.Name.ToLower().Contains(searchString.ToLower()));
                        break;
                }
            }
            return View(await appDbContext.ToListAsync());
        }

        // GET: Supplier/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var supplier = await _context.Suppliers
                .Include(s => s.Address)
                .FirstOrDefaultAsync(m => m.SupplierId == id);
            if (supplier == null)
            {
                return NotFound();
            }

            return View(supplier);
        }

        // GET: Supplier/Create
        public IActionResult Create()
        {
            ViewData["Products"] = new MultiSelectList(_context.Products, "ProductId", "Name");
            return View();
        }

        // POST: Supplier/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create ([Bind("SupplierId,Name,Phone,City,HomeNumber,Street,ZipCode," +
                                                       "Products")] EditModel newSupplier)
        {
            Supplier supplier = new Supplier()
            {
                Name = newSupplier.Name,
                Phone = newSupplier.Phone,
                SupplierId = newSupplier.SupplierId,
                Address = new Address()
                {
                    City = newSupplier.City,
                    Number = newSupplier.HomeNumber,
                    Street = newSupplier.Street,
                    ZipCode = newSupplier.ZipCode,
                }
            };
            supplier.ProductSuppliers = newSupplier.Products.Select(productId =>
                new ProductSupplier()
                {
                    SupplierId = supplier.SupplierId,
                    Supplier = supplier,
                    ProductId = productId,
                    Product = _context.Products
                        .FirstOrDefault(product => product.ProductId == productId)
                }).ToList();
            if (ModelState.IsValid)
            {
                _context.Add(supplier);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "ProductId", "Name");
            return View();
        }

        // GET: Supplier/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var currentSupplier = await _context.Suppliers.Include(supp => supp.Address)
                .Include(supp => supp.ProductSuppliers)
                .FirstOrDefaultAsync(m => m.SupplierId == id);
            if (currentSupplier == null)
            {
                return NotFound();
            }
            ViewData["Products"] = new SelectList(_context.Products, "ProductId", "Name");
            
            EditModel newSupplier = new EditModel()
            {
                SupplierId = currentSupplier.SupplierId,
                Name = currentSupplier.Name,
                Phone = currentSupplier.Phone,
                City = currentSupplier.Address.City,
                Street = currentSupplier.Address.Street,
                HomeNumber = currentSupplier.Address.Number,
                ZipCode = currentSupplier.Address.ZipCode,
                Products = new List<int>(),
            };
            foreach (var product in currentSupplier.ProductSuppliers)
            {
                newSupplier.Products.Add(product.ProductId);
            }
            return View(newSupplier);
        }

        // POST: Supplier/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SupplierId,Name,Phone,City,HomeNumber,Street,ZipCode," +
                                                    "Products")] EditModel supplier)
        {
            if (id != supplier.SupplierId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                Supplier currentSupplier = new Supplier
                {
                    Name = supplier.Name,
                    Phone = supplier.Phone,
                    SupplierId = supplier.SupplierId,
                    Address = new Address()
                    {
                        City = supplier.City,
                        Number = supplier.HomeNumber,
                        Street = supplier.Street,
                        ZipCode = supplier.ZipCode,
                    }
                };
                if (supplier.Products != null)
                {
                    currentSupplier.ProductSuppliers = supplier.Products.Select(productId =>
                        new ProductSupplier()
                        {
                            SupplierId = supplier.SupplierId,
                            ProductId = productId
                        }).ToList();
                }
                else
                {
                    currentSupplier.ProductSuppliers = new List<ProductSupplier>();
                }

                try
                {
                    _context.Suppliers.Include(s => s.ProductSuppliers)
                        .FirstOrDefault(s => s.SupplierId == supplier.SupplierId)
                        ?.ProductSuppliers.ForEach(ps => _context.Entry(ps).State = EntityState.Deleted);
                    currentSupplier.ProductSuppliers.ForEach(ps => _context.Entry(ps).State = EntityState.Added);
                    var originalSupplier = _context.Suppliers
                        .FirstOrDefault(s => s.SupplierId == currentSupplier.SupplierId);
                    _context.Entry(originalSupplier ?? throw new NullReferenceException()).State = EntityState.Detached;
                    _context.Update(currentSupplier);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SupplierExists(supplier.SupplierId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Products"] = new SelectList(_context.Products, "ProductId", "Name");
            return View(new EditModel());
        }

        // GET: Supplier/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var supplier = await _context.Suppliers
                .Include(s => s.Address)
                .FirstOrDefaultAsync(m => m.SupplierId == id);
            if (supplier == null)
            {
                return NotFound();
            }

            return View(supplier);
        }

        // POST: Supplier/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var supplier = await _context.Suppliers.Include(supp => supp.Address)
                .Include(supp => supp.ProductSuppliers)
                .FirstOrDefaultAsync(m => m.SupplierId == id); 
            var address = supplier.Address;
            var products = supplier.ProductSuppliers;
            _context.Addresses.Remove(address);
            
            foreach (var product in products)
            {
                try
                {
                    _context.ProductSuppliers.Remove(product);
                }
                catch (Exception e)
                {
                    //Noop
                }
            }
            _context.Suppliers.Remove(supplier);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SupplierExists(int id)
        {
            return _context.Suppliers.Any(e => e.SupplierId == id);
        }
    }
}