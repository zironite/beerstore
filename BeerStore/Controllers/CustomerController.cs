using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BeerStore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Rewrite.Internal.UrlActions;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace BeerStore.Controllers
{
    public class CustomerController : Controller
    {
        private readonly AppDbContext _context;

        public CustomerController(AppDbContext context)
        {
            _context = context;
        }
        
        public class EditModel
        {
            public int CustomerId { get; set; }
            
            [DataType(DataType.Text)]
            [Display(Name = "First name")]
            [StringLength(100, 
                ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", 
                MinimumLength = 2)]
            public string FirstName { get; set; }
            
            [DataType(DataType.Text)]
            [Display(Name = "Last name")]
            [StringLength(100, 
                ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", 
                MinimumLength = 2)]
            public string LastName { get; set; }
            
            [DataType(DataType.PhoneNumber)]
            [Display(Name = "Phone")]
            public string Phone { get; set; }
            
            [DataType(DataType.Text)]
            [Display(Name = "City")]
            [StringLength(100, 
                ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", 
                MinimumLength = 2)]
            public string City { get; set; }
            
            [DataType(DataType.Text)]
            [Display(Name = "Street")]
            [StringLength(100, 
                ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", 
                MinimumLength = 2)]
            public string Street { get; set; }
            
            [DataType(DataType.Text)]
            [Display(Name = "Home number")]
            public int HomeNumber { get; set; }
            
            [DataType(DataType.PostalCode)]
            [Display(Name = "Zip code")]
            public int ZipCode { get; set; }
            
            [DataType(DataType.Date)]
            [Display(Name = "Birthdate")]
            public DateTime Birthdate { get; set; }
            
            [Display(Name = "Gender")]
            [EnumDataType(typeof(GenderType))]
            public GenderType Gender { get; set; }
            
        }

        // GET: Customer
        [Authorize(Policy = "isAdmin")]
        public async Task<IActionResult> Index([FromQuery(Name = "FirstName")] string firstName,
            [FromQuery(Name = "LastName")] string lastName,
            [FromQuery(Name = "Phone")] string phone,
            [FromQuery(Name = "Gender")] int gender)
        {
            var matchingCustomers = _context.Customer.AsQueryable();
            
            if (!String.IsNullOrEmpty(firstName)) matchingCustomers = matchingCustomers.Where(x => x.FirstName.Contains(firstName));
            if (!String.IsNullOrEmpty(lastName)) matchingCustomers = matchingCustomers.Where(x => x.LastName.Contains(lastName));
            if (!String.IsNullOrEmpty(phone)) matchingCustomers = matchingCustomers.Where(x => x.Phone.Contains(phone));
            if (gender != 0) matchingCustomers = matchingCustomers.Where(x => ((int)x.Gender).Equals(gender));
            return View(await matchingCustomers.ToListAsync());
        }

        // GET: Customer/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _context.Customer
                    .Include(c => c.User)
                .FirstOrDefaultAsync(m => m.CustomerId == id);
            var userManager = _context.GetService<UserManager<User>>();
            var roleManager = _context.GetService<RoleManager<IdentityRole>>();
            
            var currentUser = userManager.GetUserAsync(User).GetAwaiter().GetResult();
            var isAdmin = userManager.IsInRoleAsync(currentUser, "Admin").GetAwaiter().GetResult();

            if (isAdmin || customer.User.UserName == User.Identity.Name)
            {
                if (customer == null)
                {
                    return NotFound();
                }

                return View(customer);
            }
            return Redirect("/Identity/Account/AccessDenied");
        }

        // GET: Customer/Create
        public IActionResult Create()
        {
            return Redirect("/Identity/Account/Register");
        }

        // POST: Customer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CustomerId,FirstName,LastName,Phone,BirthDate,Gender")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                _context.Add(customer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(customer);
        }

        // GET: Customer/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var currentCustomer = await _context.Customer.Include(cust => cust.Address)
                .Include(cust => cust.User)
                .FirstOrDefaultAsync(m => m.CustomerId == id);
            var userManager = _context.GetService<UserManager<User>>();
            var roleManager = _context.GetService<RoleManager<IdentityRole>>();
            
            var currentUser = userManager.GetUserAsync(User).GetAwaiter().GetResult();
            var isAdmin = userManager.IsInRoleAsync(currentUser, "Admin").GetAwaiter().GetResult();

            if (isAdmin || currentUser.UserName == User.Identity.Name)
            {

                if (currentCustomer == null)
                {
                    return NotFound();
                }

                var input = new EditModel()
                {
                    CustomerId = currentCustomer.CustomerId,
                    FirstName = currentCustomer.FirstName,
                    LastName = currentCustomer.LastName,
                    Phone = currentCustomer.Phone,
                    City = currentCustomer.Address.City,
                    Street = currentCustomer.Address.Street,
                    HomeNumber = currentCustomer.Address.Number,
                    ZipCode = currentCustomer.Address.ZipCode,
                    Birthdate = DateTime.ParseExact(currentCustomer.BirthDate, 
                        "dd/MM/yyyy",CultureInfo.InvariantCulture),
                    Gender = currentCustomer.Gender
                };
                return View(input);
            }
            return Redirect("/Identity/Account/AccessDenied");
        }

        // POST: Customer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditModel customer)
        {
            if (id != customer.CustomerId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var currentCustomer = _context.Customer.Include(cust => cust.Address)
                        .Include(cust => cust.User)
                        .FirstOrDefault(cust => cust.CustomerId == customer.CustomerId);
                    
                    var userManager = _context.GetService<UserManager<User>>();
                    var roleManager = _context.GetService<RoleManager<IdentityRole>>();
            
                    var currentUser = userManager.GetUserAsync(User).GetAwaiter().GetResult();
                    var isAdmin = userManager.IsInRoleAsync(currentUser, "Admin").GetAwaiter().GetResult();

                    if (isAdmin || currentUser.UserName == User.Identity.Name)
                    {
                        Debug.Assert(currentCustomer != null, nameof(currentCustomer) + " != null");
                        currentCustomer.FirstName = customer.FirstName;
                        currentCustomer.LastName = customer.LastName;
                        currentCustomer.Phone = customer.Phone;
                        currentCustomer.Address.City = customer.City;
                        currentCustomer.Address.Street = customer.Street;
                        currentCustomer.Address.Number = customer.HomeNumber;
                        currentCustomer.Address.ZipCode = customer.ZipCode;
                        currentCustomer.BirthDate = customer.Birthdate.ToString("dd/MM/yyyy");
                        currentCustomer.Gender = customer.Gender;
                        _context.Update(currentCustomer);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        return Redirect("/Identity/Account/AccessDenied");
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomerExists(customer.CustomerId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(customer);
        }

        // GET: Customer/Delete/5
        [Authorize(Policy = "delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _context.Customer
                .FirstOrDefaultAsync(m => m.CustomerId == id);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // POST: Customer/Delete/5
        [Authorize(Policy = "delete")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var customer = await _context.Customer.FindAsync(id);
            try
            {
                _context.Customer.Remove(customer);
            }
            catch (Exception e)
            {
                //Noop
            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CustomerExists(int id)
        {
            return _context.Customer.Any(e => e.CustomerId == id);
        }
    }
}
